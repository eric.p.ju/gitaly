module gitlab.com/gitlab-org/gitaly/tools/vulncheck

go 1.21

require golang.org/x/vuln v1.0.2

require (
	golang.org/x/mod v0.14.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/tools v0.17.0 // indirect
)
